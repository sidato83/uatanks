﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    // Stores a MapGenerator instance called map
    public MapGenerator map;

    // Stores audio mixers
    public AudioMixer audioMixer;
    public AudioMixer sfxMixer;

    // Stores buttons and toggles
    public Button button1;
    public Button button2;
    public Toggle toggle1;
    public Toggle toggle2;

    // Holds sprite variations for button and toggle animations
    public Sprite[] sprites;
    // Stores the number of players
    static public int numOfPlayers;
    static public bool mapOfDay;
    static public bool randomMap;

    public void Update()
    {
        // Used to feed the number of players, in real time, to the Game Manager, based on number of players selected in the menu
        GameManager.instance.numberPlayers = numOfPlayers;
    }

    // Called to set the volume level based on the menu volume slider
    public void SetVolume(float volume) => audioMixer.SetFloat("Volume", volume);

    // Called to set the sfx volume level based on the menu sfx volume slider
    public void SetSfxVolume(float volume) => sfxMixer.SetFloat("SFX Volume", volume);

    // Called to set the graphics settings
    public void SetQuality(float qualityIndex)
    {
        switch ((int) qualityIndex)
        {
            case 0:
                QualitySettings.SetQualityLevel(0);
                break;
            case 1:
                QualitySettings.SetQualityLevel(1);
                break;
            case 2:
                QualitySettings.SetQualityLevel(2);
                break;
        }
        
    }

    // Called to set the state of button one
    public void SetButtonStateOne()
    {
        // Sets the image of buttons one and two, to be opposite of each other
        button1.image.sprite = sprites[0];
        button2.image.sprite = sprites[1];

        // Sets the number of players to one (pased to Game Manager in the update)
        numOfPlayers = 1;
    }

    // Called to set the state of button two
    public void SetButtonStateTwo()
    {
        // Sets the sprite image of buttons one and two
        button2.image.sprite = sprites[0];
        button1.image.sprite = sprites[1];

        // Sets the number of players to two
        numOfPlayers = 2;
    }

    // Called to set the toggle state of toggle one
    public void SetToggleStateOne()
    {
        // Sets the image of both toggles to be opposite of each other
        toggle1.image.sprite = sprites[2];
        toggle2.image.sprite = sprites[3];

        // Sets the map of the day to true and the random map to false
        mapOfDay = true;
        randomMap = false;

        // Passes the map values to the Game Manager
        GameManager.instance.mapOfTheDay = mapOfDay;
        GameManager.instance.randomMap = randomMap;
    }

    // Called to set the toggle state of toggle two
    public void SetToggleStateTwo()
    {
        toggle2.image.sprite = sprites[2];
        toggle1.image.sprite = sprites[3];

        randomMap = true;
        mapOfDay = false;

        GameManager.instance.randomMap = randomMap;
        GameManager.instance.mapOfTheDay = mapOfDay;
    }
}
