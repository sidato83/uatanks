﻿using System;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    // GameObject array for the generated grid sections
    public GameObject[] gridPrefabs;
    // Stores an integer for row count
    public int rows;
    // Stores an int for column count
    public int cols;
    // Holds the room width
    private float roomWidth = 50.0f;
    // Holds the room height
    private float roomHeight = 50.0f;
    // Stores map of the day toggle
    public bool isMapOfTheDay;
    // Stores random map toggle
    public bool isRandomMap;
    // Holds Room class, 2D array, called grid
    public Room[,] grid;
    // Stores a value to seed the map
    public int mapSeed;

    private void Awake()
    {
        // Sets the map of the day or a random map based on settings from the menu that are passed to the Game Manager
        isMapOfTheDay = GameManager.instance.mapOfTheDay;
        isRandomMap = GameManager.instance.randomMap;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Called to set the map seed value
        SetMapSeed();
        // Called to generate the grid
        GenerateGrid();
    }

    // Function to set the map seed
    void SetMapSeed()
    {
        // Checks if both random and map of the day have been set
        if (isMapOfTheDay && isRandomMap)
        {
            // Generates a random map
            mapSeed = DateToInt(DateTime.Now);
        }

        // Checks if map of the day is enabled
        else if (isMapOfTheDay)
        {
            // Sets the map seed to a value calculated by the DateToInt function
            mapSeed = DateToInt(DateTime.Now.Date);
        }
        // Checks if random map is toggled
        else if (isRandomMap)
        {
            // Sets the map seed to a calculated DateTime.Now value
            mapSeed = DateToInt(DateTime.Now);
        }
    }

    // Function to calculate a random int value based on date and time
    public int DateToInt(DateTime dateToUse) => dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + 
        dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;

    // Function to generate a random grid prefab
    public GameObject RandomRoomPrefab() => gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];

    // Function to generate a grid
    public void GenerateGrid()
    {
        // Initializes the random number generator with the passed in seed value
        UnityEngine.Random.InitState(mapSeed);
        // Sets grid equal to a new room with passed in columns and rows
        grid = new Room[cols, rows];

        // For loop based on the number of rows
        for (int i = 0; i < rows; i++)
        {
            // Nested for loop based on the number of columns
            for (int j = 0; j < cols; j++)
            {
                // Sets float value for room width times the number of columns
                float xPosition = roomWidth * j;
                // Sets float value for room height times the number of rows
                float zPosition = roomHeight * i;

                // Sets a new room positon equal to width times columns, no change on 'Y', and height times rows
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Stores a GameObject for room clones, placed at the proper location and rotation
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity);

                // Sets the transform parent of the temp room object to this gameObject Transform
                tempRoomObj.transform.parent = this.transform;

                // Sets the name of the room with the column and row identifier
                tempRoomObj.name = "The Gauntlet" + j + "," + i;

                // New room instance
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                // Checks if this is the first row
                if (i == 0)
                {
                    // Deactivates the north door of the grid prefabs
                    tempRoom.doorNorth.SetActive(false);
                }
                // Checks if this is the last row
                else if (i == rows - 1)
                {
                    // Deactivates the south doors
                    tempRoom.doorSouth.SetActive(false);
                }
                // Deactivates north and south doors for the middle rows
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }

                // Checks if this is the first column
                if (j == 0)
                {
                    // Deactivates the east doors
                    tempRoom.doorEast.SetActive(false);
                }
                // Checks if this is the last column
                else if (j == cols - 1)
                {
                    // Deactivates the west doors
                    tempRoom.doorWest.SetActive(false);
                }
                // Deactivates east and west doors for the middle columns
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                // Sets the grid equal to tempRoom with deactivated doors
                grid[j, i] = tempRoom;
            }
        }
    }
}
