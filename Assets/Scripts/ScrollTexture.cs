﻿using UnityEngine;

public class ScrollTexture : MonoBehaviour
{
    // Variable used to store the material to scroll
    public Material scrollableMaterial;
    // Stores a Vector2 value in 'direction'
    public Vector2 direction = new Vector2(1, 0);

    // Holds the current Vector2
    private Vector2 currentOffset;

    // Start is called before the first frame update
    void Start()
    {
        // Sets the value of the current offset equal to the texture offset value of the material
        currentOffset = scrollableMaterial.GetTextureOffset("_MainTex");
    }

    // Update is called once per frame
    void Update()
    {
        // Adds the vector 2 direction to the current offset, times the input value of the vertical axis, times Time.deltaTime
        currentOffset += direction * Input.GetAxis("Vertical") * Time.deltaTime;
        // Sets the texture offset with the passed in currentOffset calculation
        scrollableMaterial.SetTextureOffset("_MainTex", currentOffset);
    }
}
