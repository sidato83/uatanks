﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    // Stores a GameObject to identify shooter
    public GameObject whoShotIt;
    // Stores damage visual effects
    public GameObject damageVFX;
    // Stores damage fx duration
    public float damageDuration = 3f;
    // Stores point values for per hit basis
    public int pointsPerHit;

    // Attaced to projectiles and activates on collision
    private void OnCollisionEnter(Collision collision)
    {
        // Instantiates damage visual effects at the projectiles position and rotation at impact
        GameObject damage = Instantiate(damageVFX, transform.position, transform.rotation);

        // Destroys the damage fx
        Destroy(damage, damageDuration);
        // Destroys the gameObject
        Destroy(gameObject);

        // When projectile collides with enemies or the player
        if (collision.collider.tag == "Enemy" || collision.collider.tag == "Player")
        {
            // When an enemy is hit, the projectile grabs the player score from tank data and adds the points per hit value
            whoShotIt.GetComponent<TankData>().playerScore += pointsPerHit;

            // Checks if the projectile is tagged as cannon
            if (gameObject.tag == "Cannon")
            {
                // Projectile grabs the tank data of the tank hit and reduces the health by the specified value set in the inspector for cannon damage
                collision.collider.GetComponent<TankData>().currentHealth -= whoShotIt.GetComponent<TankData>().cannonDamage;

                // Projectile damage fx are destroyed
                Destroy(damage, damageDuration);
                // Projectile object is destroyed
                Destroy(gameObject);
            }
            // Checks if the projectile is tagged as a bullet
            else if (gameObject.tag == "Bullet")
            {
                // Reduces the targets health by the specified bullet damage in the inspector
                collision.collider.GetComponent<TankData>().currentHealth -= whoShotIt.GetComponent<TankData>().bulletDamage;

                // Projectile damage fx are destroyed
                Destroy(damage, damageDuration);
                // Projectile object is destroyed
                Destroy(gameObject);
            }

            // Checks if the target's health is less than or equal to 0
            if (collision.collider.GetComponent<TankData>().currentHealth <= 0)
            {
                // Grabs the player score of the attacker and adds the point value of the target
                whoShotIt.GetComponent<TankData>().playerScore += collision.collider.GetComponent<TankData>().pointValue;
            }
        }
        // Checks if an object is tagged as shootable
        else if (collision.collider.tag == "Shootable")
        {
            // Projectile explodes on impact
            Instantiate(damageVFX, transform.position, transform.rotation);

            // Projectile damage fx are destroyed
            Destroy(damage, damageDuration);
            // Projectile object is destroyed
            Destroy(gameObject);
        }
    }
}
