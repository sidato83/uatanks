﻿using UnityEngine;

[System.Serializable]
public class Powerup
{
    // Stores a float to modify speed
    public float speedModifier;
    // Stores a health modifier
    public float healthModifier;
    // Stores a max health modifier
    public float maxHealthModifier;
    // Stores a cannon damage modifier
    public float cannonDamageModifier;

    // Stores a powerup duration value
    public float duration;

    // Allows powerup to be set to permanent
    public bool isPermanent;

    // Function that activates powerup and passes in tank data
    public void OnActivate(TankData target) 
    {
        // Modified target values are activated
        target.forwardSpeed += speedModifier;

        target.currentHealth += healthModifier;
        target.currentHealth = Mathf.Min(target.currentHealth, target.maxHealth);

        target.maxHealth += maxHealthModifier;

        target.cannonDamage += cannonDamageModifier;
    }

    // Function that deactivates powerup with passed in tank data
    public void OnDeactivate(TankData target)
    {
        // Modified target values are deactivated
        target.forwardSpeed -= speedModifier;
        target.currentHealth -= healthModifier;
        target.maxHealth -= maxHealthModifier;
        target.cannonDamage -= cannonDamageModifier;
    }
}
