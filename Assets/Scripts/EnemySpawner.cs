﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // Stores a Transform
    private Transform tf;
    // Stores a spawnCount integer
    private int spawnCount;
    // Stores a position for the spawner
    public Vector3 spawnerPos;
    // Stores a spawn delay
    private int spawnDelay;
    // Stores the value for the next spawn time
    private float nextSpawnTime;

    private void Awake()
    {
        // Sets 'tf' equal to this gameObject's Transform
        tf = gameObject.GetComponent<Transform>();
        // Adds the Transforms to a dynamic list in the Game Manager
        GameManager.instance.enemySpawnerTransforms.Add(tf);

        // Sets the spawn count equal to the count of the spawner transforms list
        spawnCount = GameManager.instance.enemySpawnerTransforms.Count;
    }

    private void Start()
    {
        // Sets the spawn delay from the value specified in the Game Manager
        spawnDelay = GameManager.instance.enemySpawnDelay;
        // Sets the next spawn time equal to the current time plus the specified delay
        nextSpawnTime = Time.time + spawnDelay;
    }

    private void Update()
    {
        // Checks if the number of players is more than the current amount of spawned players
        if (GameManager.instance.numberPlayers > GameManager.instance.playersSpawned)
        {
            // For each GameObject in the player prefab array in the Game Manager
            for (int i = 0; i < GameManager.instance.numberPlayers; i++)
            {
                // Function to spawn the player
                SpawnPlayer(i);
                // Sets the camera view
                CameraView view = FindObjectOfType<CameraView>();
                // Switches the view to the proper camera after the player spawns
                view.SwitchView();
            }
        }

        // Checks if the specified max number of enemies is greater than the current amount of enemies spawned
        if (GameManager.instance.maxEnemiesToSpawn > GameManager.instance.currentEnemiesSpawned)
        {
            // Function to spawn enemies
            SpawnEnemies();
        }
    }

    // Function to generate a random transform position
    Vector3 GenerateRandom(Vector3 position)
    {
        // Sets the passed in position equal to a random transform position from the enemy spawners transforms list
        position = GameManager.instance.spawnerTransformPositions[Random.Range(0, GameManager.instance.spawnerTransformPositions.Count)].position;
        return position;
    }

    // Function to spawn enemies
    void SpawnEnemies()
    {
        // For each GameObject in the enemy prefab array in the Game Manager
        foreach (GameObject enemy in GameManager.instance.enemyPrefabs)
        {
            // Checks if the spawn time has elapsed
            if (Time.time > nextSpawnTime)
            {
                // Increments the current enemy count by one
                GameManager.instance.currentEnemiesSpawned++;
                // Creates an enemy clone at a randomly generated position
                Instantiate(enemy, enemy.transform.position = GenerateRandom(enemy.transform.position), tf.rotation);
                // Resets the enemy spawn delay
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
    }

    // Function to spawn the player
    void SpawnPlayer(int i)
    {
        Vector3 position = GameManager.instance.spawnerTransformPositions[Random.Range(0, GameManager.instance.spawnerTransformPositions.Count)].position;
        Transform player = GameManager.instance.playerPrefabs[i].GetComponent<Transform>();

        // Increments the count for players spawned
        GameManager.instance.playersSpawned++;
        // Creates a player clone at a randomly generated postion
        Instantiate(GameManager.instance.playerPrefabs[i], position, tf.rotation);
        player.position = position;
    }
}
