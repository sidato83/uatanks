﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    // Stores number of players
    [Header("Number of Players")]
    public int numberPlayers;
    public int playersSpawned;

    // Displays the player score
    [Header("Score")]
    public int playerOneScore;

    // Displays health and point values for game objects
    [Header("Player Stats")]
    [SerializeField]
    private float playerOneMaxHealth;
    [SerializeField]
    private float playerOneCurrentHealth;
    [SerializeField]
    private float playerOneSpeed;

    // Store max enemies and current enemies spawned
    [Header("Enemies")]
    public int maxEnemiesToSpawn;
    public int currentEnemiesSpawned;

    // Store level map data
    [Header("Level Map")]
    public MapGenerator map;
    [SerializeField] private int mapSeed;
    public bool mapOfTheDay;
    public bool randomMap;

    // Stores a GameObject array
    [Header("Game Object Arrays")]
    public GameObject[] playerPrefabs;
    public GameObject[] enemyPrefabs;

    // Stores a TankData array
    public TankData[] gameData;
    public AIController[] aiData;

    // Variables relating to powerup spawners and enemy spawners
    [Header("Spawner Info")]
    // Dynamic list of the powerup spawns
    public List<GameObject> currentPowerSpawns = new List<GameObject>();
    // Used to store collected powerup spawns
    public List<GameObject> collectedPowerSpawns = new List<GameObject>();
    // Dynamic list of transforms for all powerup spawners
    public List<Transform> spawnerTransformPositions = new List<Transform>();
    // Dynamic list of transforms for all enemy spawners
    public List<Transform> enemySpawnerTransforms = new List<Transform>();
    // Holds the max amount of powerup spawns to generate
    public int maxPowerupSpawns;
    // Allows designers to set the respawn time on powerups
    public int powerupRespawnDelay;
    // Allows designers to set a delay on the enemy spawns
    public int enemySpawnDelay;
    // Used to toggle powerups
    public bool spawnPowerups = true;
    // Used to automatically set a spawn count based on map size
    public bool autoPowerupSpawnCount = false;

    [SerializeField]
    private Room[,] grid;

    // Game manager
    private void Awake()
    {
        // Checks if the instance is null
        if (instance == null)
        {
            // Sets the instance to this instance
            instance = this;
        }
        // If the instance is !null
        else
        {
            // Displays error message
            Debug.LogError("ERROR: There can only be one GameManager.");
            // Destroys the instance
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        mapSeed = map.mapSeed;
    }

    // Update is called once per frame
    void Update()
    {
        // Called to update game values on display
        UpdateGameValues();
    }

    // Sets the player and enemy health values and the player score
    void UpdateGameValues()
    {
        playerOneScore = gameData[0].playerScore;
        playerOneMaxHealth = gameData[0].maxHealth;
        playerOneCurrentHealth = gameData[0].currentHealth;
        playerOneSpeed = gameData[0].forwardSpeed;
    }
}
