﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    public Image[] lives;
    public Sprite heart;
    public TankData data;

    // Called to set the max health value for the healthbar ***OBTAINED FROM BRACKEYS YOUTUBE TUTORIAL: https://youtu.be/BLfNP4Sc_iA***
    public void SetMaxHealth(float health)
    {
        // Set the slider to max health
        slider.maxValue = health;
        // Set the slider value to start at max health
        slider.value = health;

        // Sets the fill color gradient based on max health
        fill.color = gradient.Evaluate(1f);
    }

    // Called to 
    public void SetHealth(float health)
    {
        // Sets the slider value based on current health
        slider.value = health;

        // Sets the fill color gradient based on the current health value
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
