﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TankMotor : MonoBehaviour
{
    // Stores TankData component
    public TankData data;

    // Stores CharacterController
    private CharacterController characterController;

    // Header for clarity in Inspector
    [Header("Transform Assignments")]
    // Variables used to store object Transforms
    public Transform tank;
    public Transform turret;
    public Transform cannonLink;
    public Transform cannonBarrel;
    public Transform guns;
    public Transform radar;
    public Transform cannonFirePosition;
    public Transform cannonFireVFXPos;
    public Transform bulletFirePosition1;
    public Transform bulletFirePosition2;

    [Header("Cannon Shell")]
    // Variables used to hold cannon GameObjects
    public GameObject cannonShell;
    public GameObject cannonExplosionVFX;
    public float explosionDuration = 2.0f;
    private GameObject shell;

    [Header("Bullet Prefab")]
    // Used to hold bullet GameObjects
    public GameObject bulletPrefab;
    public GameObject bulletFireVFX;
    public float bulletFireDuration = 1.0f;
    private GameObject bullet1;
    private GameObject bullet2;

    [Header("Audio")]
    public AudioClip cannonFire;
    [Range(0, 1)] public float cannonFireVol;
    public AudioClip bulletFire;
    [Range(0, 1)] public float bulletFireVol;

    // Variables used for setting cannon barrel recoil
    private float recoilPos = 7f;
    private float resetPos = -7f;

    // Used to hold a delay for resetting the cannon barrel
    private float resetCannon;
    // Checks if the cannon is in recoil state
    private bool cannonRecoil = false;

    void Awake() => tank = gameObject.GetComponent<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        // Stores the CharacterController
        characterController = gameObject.GetComponent<CharacterController>();
        // Sets the value for the current health equal to the pre-defined max health
        data.currentHealth = data.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        // Calls the RotateRadar() function and passes in SetVector() with the radar rotation speed passed in from TankData
        RotateRadar(SetVector(data.radarRotateSpeed));
        // Calls the CannonReset function and passes in the recoil distance from TankData
        CannonReset(data.cannonRecoilDistance);

        // Functions called to auto-destroy projectiles with time to live data passed in from TankData
        Destroy(shell, data.cannonShellTTL);
        Destroy(bullet1, data.bulletTTL);
        Destroy(bullet2, data.bulletTTL);

        // Called to destroy objects when health reaches 0
        Death();
    }

    // Funtion to control player movement
    public void Move(float speed)
    {
        // Sets the Vector3 variable 'speedVector' to transform the connected object in the forward direction
        Vector3 speedVector = tank.forward;
        // Multiplies the tanks forward vector by the speed value passed in from the function call
        speedVector *= speed;
        // Calls the SimpleMove() function of the characterController and passes in the value of the speedVector
        characterController.SimpleMove(speedVector);
    }

    // Function to control object rotation
    public void Rotate(float speed)
    {
        // Sets the Vector3 variable 'rotateVector' to rotate on the 'y' axis (up), based on the rate of speed
        // Multipled by Time.deltaTime to rotate per second instead of per frame
        Vector3 rotateVector = Vector3.up;
        rotateVector *= (speed *= Time.deltaTime);

        // Transforms the object's rotation, based on the passed in 'rotateVector' value, in local space
        tank.Rotate(rotateVector, Space.Self);
    }

    // Function to rotate enemy to target
    public bool RotateTowards(Vector3 target, float speed)
    {
        // Calculates the distance to the target
        Vector3 vectorToTarget = target - tank.position;

        // Sets the value for the amount of rotation to look at the target
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        // Checks if the AI rotation does not equal the target rotation
        if (tank.rotation != targetRotation)
        {
            // Sets the value of the tank's rotation over time
            tank.rotation = Quaternion.RotateTowards(tank.rotation, targetRotation, speed * Time.deltaTime);

            // Confirms the rotation
            return true;
        }
        // Otherwise, if no rotation is necessary
        else
        {
            // Returns a false value
            return false;
        }
    }

    // Funtion to conrol cannon elevation levels
    public void CannonElevation(float speed)
    {
        // Sets the vector3 rotate vector on the x(red) axis
        Vector3 rotateVector = Vector3.right;
        // Sets the rotateVector to rotate per second
        rotateVector *= (speed *= Time.deltaTime);

        // Sets the transform of the cannonLink to rotate in local space
        cannonLink.Rotate(rotateVector, Space.Self);
    }

    // Called to rotate the turret
    public void RotateTurret(Vector3 vector) => turret.Rotate(vector, Space.Self);

    // Called to rotate the machine guns
    public void RotateGuns(Vector3 vector)
    {
        // Sets the transform of the machine guns to rotate in local space, based on the passed in vector
        guns.Rotate(vector, Space.Self);
    }

    // Called to rotate the radar
    public void RotateRadar(Vector3 vector)
    {
        // Transforms the object's rotation, based on the passed in 'rotateVector' value, in local space
        radar.Rotate(vector, Space.Self);
    }

    // Called to set the vector for a few rotations
    public Vector3 SetVector(float speed)
    {
        // Sets the Vector3 variable 'rotateVector' to rotate on the 'z' axis (forward), based on the rate of speed
        // Multipled by Time.deltaTime to rotate per second instead of per frame
        Vector3 rotateVector = Vector3.forward;
        rotateVector *= (speed *= Time.deltaTime);

        // returns the value of the rotateVector
        return rotateVector;
    }

    // Funtion used to create recoil on the tank barrel
    public void CannonRecoil(float distance)
    {
        // Sets the x and y positions to new x,y positions
        Vector2 cannonRecoilPosition = new Vector2(0, recoilPos);

        // Translates the cannon barrel on the 'y' axis and adds the recoil position times the recoil distance passed in
        cannonBarrel.Translate(Vector2.up + cannonRecoilPosition * distance * Time.fixedDeltaTime);
        // Adds the delay for the cannon barrel reset
        resetCannon = Time.time + data.cannonPositionResetDelay;

        // Sets the recoil position to true
        cannonRecoil = true;
    }

    // Used to reset the cannon barrel to the original position
    public void CannonReset(float distance)
    {
        // Sets the x,y position for the barrel
        Vector2 cannonResetPosition = new Vector2(0, resetPos);

        // Checks if the recoil value is true and the reset delay has passed
        if (cannonRecoil && Time.time >= resetCannon)
        {
            // Translates the cannon barrel to its original position
            cannonBarrel.Translate(-Vector2.up + cannonResetPosition * distance * Time.fixedDeltaTime);
            // Sets the recoil value to false
            cannonRecoil = false;
        }
    }

    // Used to fire the cannon
    public void FireCannon(float force)
    {
        // Creates a clone by instantiating the cannonShell prefab at the location and rotation of the firing position
        shell = Instantiate(cannonShell, cannonFirePosition.position, cannonFirePosition.rotation);
        shell.GetComponent<Projectile>().whoShotIt = gameObject;
        // Gets the Rigidbody component of the clone and adds negative force on the 'y' axis
        shell.GetComponent<Rigidbody>().velocity = -force * cannonFirePosition.up;

        GameObject explosion = Instantiate(cannonExplosionVFX, cannonFireVFXPos.position, cannonFirePosition.rotation);

        Destroy(explosion, explosionDuration);

        AudioSource.PlayClipAtPoint(cannonFire, Camera.main.transform.position, cannonFireVol);
    }

    // Used to allow continuous fire of the machine guns
    public IEnumerator FireContinuously(float force)
    {
        // Checks if the condition is met to start the coroutine (key down)
        while (true)
        {
            // Instantiates bullet prefabs for both gun barrels
            bullet1 = Instantiate(bulletPrefab, bulletFirePosition1.position, bulletFirePosition1.rotation);
            bullet1.GetComponent<Projectile>().whoShotIt = gameObject;
            GameObject bulletFire1 = Instantiate(bulletFireVFX, bulletFirePosition1.position, Quaternion.LookRotation(-bulletFirePosition1.up));
            

            bullet2 = Instantiate(bulletPrefab, bulletFirePosition2.position, bulletFirePosition2.rotation);
            bullet2.GetComponent<Projectile>().whoShotIt = gameObject;
            GameObject bulletFire2 = Instantiate(bulletFireVFX, bulletFirePosition2.position, Quaternion.LookRotation(-bulletFirePosition2.up));

            // Sets the velocity of the bullets from both barrels
            bullet1.GetComponent<Rigidbody>().velocity = -force * bulletFirePosition1.up;
            bullet2.GetComponent<Rigidbody>().velocity = -force * bulletFirePosition2.up;

            Destroy(bulletFire1, bulletFireDuration);
            Destroy(bulletFire2, bulletFireDuration);

            AudioSource.PlayClipAtPoint(bulletFire, Camera.main.transform.position, bulletFireVol);

            // Sets a delay in the function to allow for different rates of fire
            yield return new WaitForSeconds(data.bulletFiringDelay);
        }
    }

    // Used to determine kill sequence
    public void Death()
    {
        // Checks if the current health has dropped to 0 or less
        if (data.currentHealth <= 0.0f)
        {
            // Checks if the game object that has lost health is tagged as an enemy
            if (gameObject.CompareTag("Enemy"))
            {
                // Sets the transform position of the enemy tank to a new random location
                tank.position = GameManager.instance.enemySpawnerTransforms[Random.Range(0, GameManager.instance.enemySpawnerTransforms.Count)].position;
                // Resets the health of the enemy back to max
                data.currentHealth = data.maxHealth;
            }
            // Runs if the game object is not tagged as an enemy
            else
            {
                // The life count is reduced for the tank
                data.currentLives--;
                // Health is replenished
                data.currentHealth = data.maxHealth;
                // Sets the transform position of the tank to a new random location
                tank.position = GameManager.instance.enemySpawnerTransforms[Random.Range(0, GameManager.instance.enemySpawnerTransforms.Count)].position;

                // Checks if the game object's current lives have dropped below 1
                if (data.currentLives < 1)
                {
                    // Sets the game object to inactive
                    gameObject.SetActive(false);
                    // Reduces the player count of the Game Manager
                    GameManager.instance.numberPlayers--;

                    // Checks if the game object is tagged as a player
                    if (gameObject.CompareTag("Player"))
                    {
                        // Checks if the number of players managed by the Game Manager is less than 1
                        if (GameManager.instance.numberPlayers < 1)
                        {
                            // Loads the game over scene once all players have died
                            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                        }
                    }
                }
            }            
        }
    }
}
