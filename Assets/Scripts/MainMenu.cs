﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Called to navigate through game scenes.  Grabs the current scene index and adds one to it
   public void PlayGame() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    // Called to return to the main start menu by directly accessing the first build index
    public void ReturnToMain() => SceneManager.LoadScene(0);

    // Called to quit the game
    public void QuitGame()
    {
        // Debug log to show that quit is functioning in the console
        Debug.Log("QUIT");
        // Quits the application
        Application.Quit();
    }
}
