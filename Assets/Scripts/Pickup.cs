﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    // Stores Powerup instance
    public Powerup powerup;
    // Stores audioclip for pickup feedback
    public AudioClip feedback;
    // Slider for feedback volume
    [Range(0, 1)] public float feedbackVol;

    // Stores rotation speed for powerup animation
    public float rotationSpeed = 100.0f;
    // Stores this pickup's Transform
    private Transform tf;

    private void Start()
    {
        // Sets 'tf' equal to this gameObjects Transform
        tf = gameObject.GetComponent<Transform>();
    }

    private void Update()
    {
        // Called to rotate the powerup icon
        RotatePowerup();
    }

    // Function to perform actions on trigger enter
    public void OnTriggerEnter(Collider other)
    {
        // Creates a PowerupController instance and sets it equal to PowerupController
        PowerupController powCon = other.GetComponent<PowerupController>();

        // Checks if the object that entered the trigger has a powerup controller
        if (powCon != null)
        {
            // Adds the powerup to the controller
            powCon.Add(powerup);

            // Checks if feedback is not null
            if (feedback != null)
            {
                // Plays the audioclip at the point of trigger, with the main camera as the listener, at the specified volume
                AudioSource.PlayClipAtPoint(feedback, Camera.main.transform.position, feedbackVol);
            }

            // Called to reduce the current powerup spawn count
            ReducePowerupSpawnCount();
            // Destroys the powerup icon
            Destroy(gameObject);
        }
    }

    // Function to reduce the powerup spawn count
    void ReducePowerupSpawnCount()
    {
        // For each GameObject in the current power spawns list in the Game Manager
        foreach (GameObject spawnedPickup in GameManager.instance.currentPowerSpawns)
        {
            // Checks if the spawned pickup no longer exists
            if (spawnedPickup == null)
            {
                // Adds the powerup to the collected powerups list
                GameManager.instance.collectedPowerSpawns.Add(spawnedPickup);
            }
        }

        // For each GameObject in the collected powerups list
        foreach (GameObject collectedPickup in GameManager.instance.collectedPowerSpawns)
        {
            // Removes the collected powerup
            GameManager.instance.currentPowerSpawns.Remove(collectedPickup);
        }

        // Clears the collected powerups list
        GameManager.instance.collectedPowerSpawns.Clear();
    }

    // Function to rotate the powerup icon
    void RotatePowerup()
    {
        // Sets the icon rotation speed over time
        float iconRotation = rotationSpeed * Time.fixedDeltaTime;
        // Rotates the icon on the 'Y' axis per second
        tf.Rotate(new Vector3(0f, iconRotation, 0f));
    }
}
