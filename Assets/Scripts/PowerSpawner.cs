﻿using UnityEngine;

public class PowerSpawner : MonoBehaviour
{
    // GameObject array for powerup pickups
    public GameObject[] pickupPrefab;

    [Header("Power-Spawner Variables")]
    // Stores a delay for powerup respawns
    private int spawnDelay;

    // Holds the pickup clone GameObject
    public GameObject spawnedPickup;
    // Stores the amount of generated spawners
    public int spawnCount;
    // Sets a max amount of powerup spawns
    public float maxSpawns;
    // Holds a Vector3 for spawner positions
    public Vector3 spawnerPos;
    // Holds the value for the next spawn time
    private float nextSpawnTime;
    // Stores the spawner transform
    private Transform tf;
    

    private void Awake()
    {
        // Sets the transform to this gameObject's transform
        tf = gameObject.GetComponent<Transform>();
        // Adds the transform values to the Transform list in the GameManager
        GameManager.instance.spawnerTransformPositions.Add(tf);

        // Sets the spawn count to the count of the Transform Position list
        spawnCount = GameManager.instance.spawnerTransformPositions.Count;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Sets the spawn delay to the value specified by designers in the GameManager
        spawnDelay = GameManager.instance.powerupRespawnDelay;
        // Sets the next spawn time equal to the current time plus the specified delay
        nextSpawnTime = Time.time + spawnDelay;

        // Calls the function to calculate the max number of powerup spawns
        CalculateMaxPowerupSpawns();
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if a spawned pickup doesn't exist
        if (spawnedPickup == null)
        {
            // Checks if the spawn time has been met
            if (Time.time > nextSpawnTime)
            {
                // Checks if the max amount of spawns is greater than the current spawn count
                if (maxSpawns > GameManager.instance.currentPowerSpawns.Count)
                {
                    // Creates a random spawnedPickup clone, chosen from the pickup array, and instantiates it at a random position from the transform position list 
                    spawnedPickup = Instantiate(pickupPrefab[Random.Range(0, pickupPrefab.Length)], GenerateRandom(spawnerPos) // Called to generate a random position
                        , tf.rotation);
                    // Resets the spawn delay timer
                    nextSpawnTime = Time.time + spawnDelay;

                    // Adds the spawnedPickup to the current spawns list
                    GameManager.instance.currentPowerSpawns.Add(spawnedPickup);
                }
            }
        }
        // If the spawned pickup still exists
        else
        {
            // Resets the spawntime delay
            nextSpawnTime = Time.time + spawnDelay;
        }
    }

    // Function used to calculate the max amount of powerup spawns for any given map
    void CalculateMaxPowerupSpawns()
    {
        // Stores the count of the transform positions list
        float count = GameManager.instance.spawnerTransformPositions.Count;

        // Checks if the option to spawn powerups has been disabled
        if (GameManager.instance.spawnPowerups == false)
        {
            // Sets the max spawns to 0
            maxSpawns = 0;
        }
        // If spawns haven't been disabled and auto-spawn is enabled
        else if (GameManager.instance.autoPowerupSpawnCount == true)
        {
            // If the count of transform positions is less than 6
            if (count < 6)
            {
                // Max spawns is set to 1
                maxSpawns = 1;
            }
            // If the count is equal to 6 and less than 9
            else if (count >= 6 && count < 9)
            {
                // Max spawns is set to 2
                maxSpawns = 2;
            }
            // If the count is equal to 9 and less than 20
            else if (count >= 9 && count < 20)
            {
                // Max spawns is set to 3
                maxSpawns = 3;
            }
            // If the count is equal to 20 and less than 80
            else if (count >= 20 && count < 80)
            {
                // Max spawns is set to 4
                maxSpawns = 4;
            }
            // Anything equal to 80 or more
            else
            {
                // Max spawns is set to 5% of the generated transforms
                maxSpawns = count * 0.05f;
            }
        }
        // If spawns haven't been disabled and auto-spawn isn't enabled
        else
        {
            // Max spawns can be specified by designers in the Game Manager
            maxSpawns = GameManager.instance.maxPowerupSpawns;
        }
    }

    // Function to generate a random transform postion
    private Vector3 GenerateRandom(Vector3 position)
    {
        // Sets the passed in position equal to a random transfom position from the transform position list
        position = GameManager.instance.spawnerTransformPositions[Random.Range(0, spawnCount)].position;
        // Returns the position value
        return position;
    }
}
