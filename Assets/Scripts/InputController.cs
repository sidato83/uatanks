﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    public TankMotor motor;
    public TankData data;
    public CameraView view;

    // Enum set for input options
    public enum InputScheme { WASD, ArrowKeys, Controller };
    // Pre-sets the input scheme to WASD
    public InputScheme input = InputScheme.WASD;

    // Stores the delay on the cannon fire
    private float cannonFireDelay;
    // Stores the coroutine
    Coroutine firingCoroutine;

    // Update is called once per frame
    void Update()
    {
        // Checks if the input is set to WASD
        if (input == InputScheme.WASD)
        {
            // Calls the KeyboardInput function
            KeyboardInput();

            // Checks if time is greater than the cannon fire delay and the space key is down
            if (Time.time >= cannonFireDelay && Input.GetKey(KeyCode.Space))
            {
                // Calls CannonRecoil from TankMotor and passes in the recoil distance from TankData
                motor.CannonRecoil(data.cannonRecoilDistance);
                // Calls the FireCannon funtion from TankMotor and passes in the cannon shell force
                motor.FireCannon(data.cannonShellForce);
                // Adds to the cannon fire delay timer
                cannonFireDelay = Time.time + data.cannonFireDelayTimer;
            }

            // Called to fire the machine gun
            FireGun();
        }
        else if (input == InputScheme.ArrowKeys)
        {
            ArrowInput();

            // Checks if time is greater than the cannon fire delay and the space key is down
            if (Time.time >= cannonFireDelay && Input.GetKey(KeyCode.RightShift))
            {
                // Calls CannonRecoil from TankMotor and passes in the recoil distance from TankData
                motor.CannonRecoil(data.cannonRecoilDistance);
                // Calls the FireCannon funtion from TankMotor and passes in the cannon shell force
                motor.FireCannon(data.cannonShellForce);
                // Adds to the cannon fire delay timer
                cannonFireDelay = Time.time + data.cannonFireDelayTimer;
            }

            // Called to fire the machine gun
            FireGun();
        }

        
    }

    // Used to define keyboard input keys
    void KeyboardInput()
    {
        // Switch cases based on input method
        switch (input)
        {
            // For InputScheme WASD the following keys are defined
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    // Called to move the tank forward
                    motor.Move(data.forwardSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    // Called to move the tank in reverse
                    motor.Move(-data.reverseSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    // Called to rotate the tank in the positive
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    // Called to rotate the tank in the negative
                    motor.Rotate(-data.turnSpeed);
                }

                if (Input.GetKey(KeyCode.R))
                {
                    // Moves the cannon barrel up
                    motor.CannonElevation(-data.cannonElevateSpeed);
                }
                if (Input.GetKey(KeyCode.C))
                {
                    // Moves the cannon barrel down
                    motor.CannonElevation(data.cannonElevateSpeed);
                }
                
                if (Input.GetKey(KeyCode.Keypad2))
                {
                    // Rotates the turret right
                    motor.RotateTurret(motor.SetVector(data.turretRotateSpeed));
                }
                if (Input.GetKey(KeyCode.Keypad1))
                {
                    // Rotates the turret left
                    motor.RotateTurret(motor.SetVector(-data.turretRotateSpeed));
                }

                if (Input.GetKey(KeyCode.E))
                {
                    // Rotates the machine gun right
                    motor.RotateGuns(motor.SetVector(data.machineGunRotateSpeed));
                }
                if (Input.GetKey(KeyCode.Q))
                {
                    // Rotates the machine gun left
                    motor.RotateGuns(motor.SetVector(-data.machineGunRotateSpeed));
                }

                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    // Switches the camera view
                    view.SwitchView();
                }
                break;
        }
    }

    void ArrowInput()
    {
        // Switch cases based on input method
        switch (input)
        {
            // For InputScheme ArrowKeys the following keys are defined
            case InputScheme.ArrowKeys:
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    // Called to move the tank forward
                    motor.Move(data.forwardSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    // Called to move the tank in reverse
                    motor.Move(-data.reverseSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    // Called to rotate the tank in the positive
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    // Called to rotate the tank in the negative
                    motor.Rotate(-data.turnSpeed);
                }

                if (Input.GetKey(KeyCode.I))
                {
                    // Moves the cannon barrel up
                    motor.CannonElevation(-data.cannonElevateSpeed);
                }
                if (Input.GetKey(KeyCode.K))
                {
                    // Moves the cannon barrel down
                    motor.CannonElevation(data.cannonElevateSpeed);
                }

                if (Input.GetKey(KeyCode.L))
                {
                    // Rotates the turret right
                    motor.RotateTurret(motor.SetVector(data.turretRotateSpeed));
                }
                if (Input.GetKey(KeyCode.J))
                {
                    // Rotates the turret left
                    motor.RotateTurret(motor.SetVector(-data.turretRotateSpeed));
                }

                if (Input.GetKey(KeyCode.O))
                {
                    // Rotates the machine gun right
                    motor.RotateGuns(motor.SetVector(data.machineGunRotateSpeed));
                }
                if (Input.GetKey(KeyCode.U))
                {
                    // Rotates the machine gun left
                    motor.RotateGuns(motor.SetVector(-data.machineGunRotateSpeed));
                }

                if (Input.GetKeyDown(KeyCode.RightControl))
                {
                    // Switches the camera view
                    view.SwitchView();
                }
                break;
        }
    }

    // Used to fire the machine gun
    public void FireGun()
    {
        // Checks if the 'X' key is held down
        if (Input.GetKeyDown(KeyCode.X))
        {
            // Starts the firingCoroutine and passes in the FireContinuously function with the bullet force passed in
            firingCoroutine = StartCoroutine(motor.FireContinuously(data.bulletForce));
        }
        // Checks if the 'X' key is released
        if (Input.GetKeyUp(KeyCode.X))
        {
            // Stops the firingCoroutine
            StopCoroutine(firingCoroutine);
        }
    }
}
