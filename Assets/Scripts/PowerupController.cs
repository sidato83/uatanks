﻿using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    // Stores a TankData instance
    public TankData data;
    // Stores a dynamic list of powerups
    public List<Powerup> powerups;

    void Start()
    {
        // Initializes the powerups list to an empty list
        powerups = new List<Powerup>();
    }

    void Update()
    {
        // Creates a new Powerup list for expired powerups
        List<Powerup> expiredPowerups = new List<Powerup>();

        // For each Powerup instance in the powerups list
        foreach (Powerup power in powerups)
        {
            // Decrements the power instance duration (set in inspector) over time
            power.duration -= Time.deltaTime;

            // Checks if the time to live has expired
            if (power.duration <= 0)
            {
                // Adds the expired powerup to the expired powerups list
                expiredPowerups.Add(power);
            }
        }

        // For each expired powerup in the expired powerups list
        foreach (Powerup power in expiredPowerups)
        {
            // Deactivates the affects of the powerup and passes in the affected tank data
            power.OnDeactivate(data);
            // Removes the expired power from the powerups list
            powerups.Remove(power);
        }

        // Clears the expired powerups from the list
        expiredPowerups.Clear();
    }

    // Function to add passed in powerup
    public void Add(Powerup powerup)
    {
        // Calls OnActivate from the powerup instance and passes in the tank data
        powerup.OnActivate(data);

        // Checks if the powerup is not set to permanent
        if (!powerup.isPermanent)
        {
            // Adds the powerup to the powerups list
            powerups.Add(powerup);
        }
    }
}
