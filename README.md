### UATanks : GPE205 ###
Unity-3D, Game Programmming in C#

### How do I get set up? ###
Controls:

W: Forward
S: Reverse
A: Left
D: Right

Q: Machine Gun - Left

E: Machine Gun - Right
X: Machine Gun - Fire

Up Arrow: Cannon Up

Down Arrow: Cannon Down
Left Arrow: Turret Left
Right Arrow: Turret Right

Space Bar: Cannon Fire

Tab: Switch Camera View

### Enemy States ###
Patrol: Enemy will follow a set path of waypoints

Three path options are currently available
Stop: Enemy stops at the end of the set waypoints
Loop: Enemy will proceed through waypoints and then loop around and start over
Ping-pong: Enemy will proceed through waypoints, and then proceed back through the waypoints in the opposite direction.
Chase: Enemy will chase after the player and attack

Flee: Enemy will run from the player when health drops to low

Rest: Enemy will stop at a safe distance and regain health

### Contribution guidelines ###
Weekly Milestones to meet project objectives

### Who do I talk to? ###
Repo owner or admin